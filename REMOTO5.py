#Questão1

def quant_palavras(frase):
    val = frase.split()
    lin = len(val)
    return int(lin)

#Questão2
'''Função que identifica a quantidade de frases de uma string'''
'''String->Int'''
def conta_frases(frase):
    QtFrases = 0
    if '.' in frase:
        QtFrases = QtFrases + int(frase.count('.'))
    if '..' in frase:
        QtFrases = QtFrases - 2*(frase.count('..'))
    if '!' in frase:
        QtFrases = QtFrases + int(frase.count('!'))
    if '?' in frase:
        QtFrases = QtFrases + int(frase.count('?'))
    
    return QtFrases

#Questão3
"""Uma lista que dados 3 itens de cada lista1 e 2 (ignora qualquer item a mais), retorna uma 3 lista com os itens intercalados"""
'''list/list->list'''
def intercala(lista1, lista2):

    L3 = [lista1[0],lista2[0],lista1[1],lista2[1],lista1[2],lista2[2]]
    
    return L3

#Questão4
'''Função que retira a pontuação de uma string
	String -> String'''
def retira_pontuacao(frase): 
    if '-' in frase:
        frase = frase.replace('-',' ')
    if ',' in frase:
        frase= frase.replace(',',' ')
    if ':' in frase:
        frase = frase.replace(':',' ')
    if ';' in frase:
        frase = frase.replace(';',' ')
    if '.' in frase:
        frase = frase.replace('.',' ')
    if '...' in frase:
        frase = frase.replace('...',' ')
    if '!' in frase:
        frase = frase.replace('!',' ')
    if '?' in frase:
        frase = frase.replace('?',' ')
        
    return frase

#Questão5
'''Função que inverte a ordem de uma frase, tirando pontuação e letras maiúsculas'''
'''String->String'''
def inverte(frase):
    
    frase = retira_pontuacao(frase)
    val = frase.split()
    '''lav = list(reversed(val))'''
    #versão usando reversed
    lav = list(val[-1:-(len(val)+1):-1])
    #Versão usando fatiamento de lista
    StringFinal = (' '.join(lav))
    return StringFinal.lower()

#Questão6
def piramide_num(n1, n2):
    """Função que retorna uma lista de inteiros em formato pirâmide.
       int, int->list"""

    lista1 = [n1]

    if  n2 < n1 :
        return list(range(n1, n2, -1)) + list(range(n2, n1)) + lista1
    elif n1 < n2:
        return list(range(n1, n2)) + list(range(n2, n1, -1)) + lista1
    else:
        return lista1

#Questão7

"""Função que retorna se o colchão passa pela porta
    dado as medidas do colchão.
    list, float, float--> bool"""
    
def colchao(medidas, H, L):
    
    if medidas[1] <= H:
        return True
    if medidas[1] <= L:
        return True
    if medidas[2] <= H:
        return True
    if medidas[2] <= L:
        return True
    else:
        return False
